package com.devcamp.vouchersvalidate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VouchersvalidateApplication {

	public static void main(String[] args) {
		SpringApplication.run(VouchersvalidateApplication.class, args);
	}

}
