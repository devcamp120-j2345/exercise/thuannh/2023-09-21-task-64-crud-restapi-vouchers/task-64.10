package com.devcamp.vouchersvalidate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.vouchersvalidate.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long>{
    
}
